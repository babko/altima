<?php

/**
 * implementation of hook_panels_layouts()
 */
// Plugin definition
$plugin = array(
  'title' => t('Single bootstrap column'),
  'category' => t('Columns: 1'),
  'icon' => 'altest_onecol.png',
  'theme' => 'altest_onecol',
  'css' => 'altest_onecol.css',
  'regions' => array(
    'top' => t('Top'),
    'middle1' => t('Middle'),
    'middle2' => t('Middle fluid'),
    'bottom' => t('Bottom'),
  ),
);
